import React from "react";

//COMPONENTS
import HeaderLeft from "./headerLeft";
import HeaderRight from "./headerRight";

const MiloHeader = () => {
  return (
    <React.Fragment>
      <div className="milo_header_box milo_wrap">
        <HeaderLeft />
        <HeaderRight />
      </div>
      <style jsx>{`
        .milo_header_box {
          display: flex;
          justify-content: center;
        }

        @media screen and (max-width: 650px) {
          .milo_header_box {
            flex-wrap: wrap-reverse;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default MiloHeader;
