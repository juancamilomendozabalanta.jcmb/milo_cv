import { useContext } from "react";

import { MiloContext } from "../../context";

const useHeader = () => {
  const [state, setState] = useContext(MiloContext);

  return {
    me: state.me,
  };
};

export default useHeader;
