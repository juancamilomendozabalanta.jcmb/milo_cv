import React from 'react';

//HOOKS
import useHeader from '../headerHook';

//COLORS
import { COLORS } from '../../../utils';

const HeaderRight = () => {
  const { me } = useHeader();
  return (
    <React.Fragment>
      <div className='header_right_box'>
        <img
          className='header_right_image'
          src={me.photo}
          alt='Picture of the author'
        />
        <div className='header_right_square' />
      </div>
      <style jsx>{`
        .header_right_box {
          position: relative;
          width: 90%;
          display: flex;
          justify-content: flex-end;
        }
        .header_right_square {
          width: 235px;
          height: 218px;
          border: solid 4px ${COLORS.DARK_YELLOW};
        }
        .header_right_image {
          position: absolute;
          top: -132px;
          width: 83%;
          max-width: 250px;
          right: 3px;
        }
        @media screen and (max-width: 650px) {
          .header_right_image {
            width: 236px;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default HeaderRight;
