import React from "react";

//HOOKS
import useHeader from "../headerHook";

//COLORS
import { COLORS } from "../../../utils";

const HeaderLeft = () => {
  const { me } = useHeader();
  return (
    <React.Fragment>
      <div className="header_left_box">
        <p className="header_left_hi">Hello</p>
        <p className="header_left_im">I'm {me.nickname}</p>
        <p className="header_left_role">{me.role}</p>
        <p className="header_left_resume">{me.resume}</p>
      </div>
      <style jsx>{`
        .header_left_box {
          display: flex;
          flex-flow: column;
          width: 100%;
        }
        .header_left_hi {
          font-size: 1.4rem;
          color: ${COLORS.GRAY};
          font-family: sans-serif;
        }
        .header_left_im {
          padding: 15px 0px;
          font-size: 1.8rem;
          font-weight: bold;
          color: ${COLORS.DARK_YELLOW};
        }
        .header_left_role {
          font-size: 1.3rem;
          color: ${COLORS.BLUE};
          font-weight: 500;
          margin-bottom: 10px;
        }
        .header_left_resume {
          font-size: 1.2rem;
          line-height: 1.3rem;
          padding: 15px 0px;
          color:${COLORS.BLUE};
          background-color: aliceblue;
          padding: 10px;
          font-size: 1.0rem;
        }
        @media screen and (max-width: 650px) {
          .header_left_box {
            margin: 20px 0px;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default HeaderLeft;
