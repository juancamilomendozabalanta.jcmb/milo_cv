import React from "react";

//COLORS
import { COLORS } from "../../utils";

const SectionTitle = ({ title }) => {
  return (
    <React.Fragment>
      <div className="section_box">
        <p className="section_title">{title}</p>
        <span className="section_span" />
      </div>
      <style jsx>{`
        .section_box {
          display: flex;
          width: 100%;
          flex-flow: column;
        }
        .section_title {
          color: ${COLORS.DARK_GRAY};
          font-size: 1.3rem;
          font-weight: bold;
          padding-bottom: 8px;
        }
        .section_span {
          width: 45px;
          border-bottom: 4px solid ${COLORS.DARK_YELLOW};
        }
      `}</style>
    </React.Fragment>
  );
};

export default SectionTitle;
