import MailIcon from "@material-ui/icons/Mail";
import GitHubIcon from "@material-ui/icons/GitHub";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import SportsSoccerIcon from "@material-ui/icons/SportsSoccer";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import LocalMoviesIcon from "@material-ui/icons/LocalMovies";

const getIcon = (name) => {
  let component;
  switch (name) {
    case "MailIcon":
      component = <MailIcon></MailIcon>;
      break;
    case "GitHubIcon":
      component = <GitHubIcon></GitHubIcon>;
      break;
    case "LinkedInIcon":
      component = <LinkedInIcon></LinkedInIcon>;
      break;
    case "SportsSoccerIcon":
      component = <SportsSoccerIcon></SportsSoccerIcon>;
      break;
    case "FastfoodIcon":
      component = <FastfoodIcon></FastfoodIcon>;
      break;
    case "LocalMoviesIcon":
      component = <LocalMoviesIcon></LocalMoviesIcon>;
      break;
    default:
      break;
  }
  return component;
};

export { getIcon };
