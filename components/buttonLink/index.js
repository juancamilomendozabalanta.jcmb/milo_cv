import React from "react";

const ButtonLink = ({ text, blColor, handleFunction }) => {
  return (
    <React.Fragment>
      <div className="button_link_box">
        <span className="button_link_box_text" onClick={() => handleFunction()}>
          {text}
        </span>
      </div>
      <style jsx>{`
        .button_link_box {
          font-size: 0.7rem;
          font-weight: bold;
          text-align: end;
          width: 100%;
          color: ${blColor};
        }
        .button_link_box_text {
          cursor: pointer;
          height: 5px;
          padding: 3px;
        }
        .button_link_box_text:hover {
          border-bottom: 1px solid ${blColor};
        }
      `}</style>
    </React.Fragment>
  );
};

export default ButtonLink;
