import React from "react";

//COMPONENTS
import SectionTittle from "../../sectionTittle";

const ContactAndHobbieHeader = () => {
  return (
    <React.Fragment>
      <div className="chh_header_box">
        <SectionTittle title={"A little more"} />
      </div>
      <style jsx>{`
        .chh_header_box {
          display: flex;
          margin: 0px 0px 20px 0px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ContactAndHobbieHeader;
