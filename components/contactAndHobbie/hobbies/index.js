import React from "react";

//HOOKS
import useContactAndHobbieHook from "../contactAndHobbieHook";

//COMPONENTS
import ChCard from "../chCard";

//COLORS
import { COLORS } from "../../../utils";

const Hobbies = () => {
  const { hoobies } = useContactAndHobbieHook();
  return (
    <React.Fragment>
      <div className="hoobies_box">
        <p className="hoobies_box_title">Hoobies</p>
        <ul className="hoobies_box_list">
          {hoobies.map((ele, i) => {
            return <ChCard key={i} info={ele} />;
          })}
        </ul>
      </div>
      <style jsx>{`
        .hoobies_box {
          display: flex;
          width: 100%;
          flex-flow: column;
          margin: 5px;
          border-radius: 4px;
          background-color: white;
          color: white;
          align-items: center;
          border-left: 1px solid ${COLORS.BLUE};
          border-bottom: 1px solid ${COLORS.BLUE};
          border-right: 1px solid ${COLORS.BLUE};
        }
        .hoobies_box_list {
          display: flex;
          padding: 20px 5px;
        }
        .hoobies_box_title {
          font-size: 0.9rem;
          padding: 10px 0px;
          text-align: center;
          background-color: ${COLORS.BLUE};
          font-weight: bold;
          text-transform: uppercase;
          letter-spacing: 0.1rem;
          width: 100%;
          border-radius: 3px 3px 0px 0px;
        }

        @media screen and (max-width: 450px) {
          .hoobies_box {
            width: 320px;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default Hobbies;
