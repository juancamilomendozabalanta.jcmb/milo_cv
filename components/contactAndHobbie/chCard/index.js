import React from "react";

//COMPONENTS
import { getIcon } from "../../icons";

//COLORS
import { COLORS } from "../../../utils";

const ChCard = ({ info }) => {
  return (
    <React.Fragment>
      <a
        className="chcard_box"
        href={info.url ? info.url : ""}
        target="_blank"
        rel="noopener noopener noreferrer"
      >
        <span className="chcard_box_logo">{getIcon(info.logo)}</span>
        <p className="chcard_box_name">{info.name}</p>
      </a>
      <style jsx>{`
        .chcard_box {
          display: flex;
          margin: 5px;
          border-radius: 4px;
          background-color: ${COLORS.BLUE};
          padding: 8px 5px;
          align-items: center;
          cursor: pointer;
        }
        .chcard_box_logo {
          padding: 0px 5px;
        }
        .chcard_box_name {
          font-size: 0.9rem;
          padding-right: 5px;
        }
        .chcard_box:hover {
          filter: ${info.url ? "brightness(150%)" : ""};
        }
      `}</style>
    </React.Fragment>
  );
};

export default ChCard;
