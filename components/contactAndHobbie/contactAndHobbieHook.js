import { useContext } from "react";

import { MiloContext } from "../../context";

const useContactAndHobbieHook = () => {
  const [state, setState] = useContext(MiloContext);

  return {
    contacts: state.contacts,
    hoobies: state.hoobies
  };
};

export default useContactAndHobbieHook;
