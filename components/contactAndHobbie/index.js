import React from "react";

//COMPONENTS
import Contacts from "./contacts";
import Hobbies from "./hobbies";
import ContactAndHobbieHeader from "./contactAndHobbieHeader";


const ContactAndHobbie = () => {
  return (
    <React.Fragment>
      <section className="contact_hobbie_box">
        <div className="contact_hobbie_wrap milo_wrap">
          <ContactAndHobbieHeader/>
          <Contacts />
          <Hobbies />
        </div>
      </section>
      <style jsx>{`
        .contact_hobbie_box {
          display: flex;
          width: 100%;
          padding: 30px 0px;
          background-color: aliceblue;
          justify-content: center;
        }
        .contact_hobbie_wrap {
          display: flex;
          flex-flow: column;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ContactAndHobbie;
