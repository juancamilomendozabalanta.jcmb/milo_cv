import React from "react";

//COMPONENTS
import SectionTittle from "../../sectionTittle";

const ExperienceHeader = () => {
  return (
    <React.Fragment>
      <div className="experience_header_box">
        <SectionTittle title={"Experience"} />
      </div>
      <style jsx>{`
        .experience_header_box {
          display: flex;
          margin: 0px 0px 20px 0px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceHeader;
