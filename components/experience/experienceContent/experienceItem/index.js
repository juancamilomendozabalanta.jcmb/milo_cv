import React from "react";

//COMPONENTS
import ExperienceCompanies from "./experienceCompanies";

//COLORS
import { COLORS } from "../../../../utils";

const ExperienceItem = ({ info }) => {
  return (
    <React.Fragment>
      <div className="experience_item">
        <p className="experience_year">{info.year}</p>
        <ul className="experience_companies">
          {info.companies.map((ele, i) => {
            return <ExperienceCompanies key={i} info={ele} />;
          })}
        </ul>
      </div>
      <style jsx>{`
        .experience_item {
          display: flex;
          width: 100%;
          min-width: 350px;
          min-height: 150px;
          padding: 0px 20px 20px 20px;
          background-color: white;
          margin: 10px;
          border-radius: 5px;
          flex-flow: column;
        }
        .experience_item:hover{
          box-shadow: 0px 0px 3px 0px #3d4b5830;        
        }
        .experience_year {
          font-weight: bold;
          font-size: 0.95rem;
          font-family: sans-serif;
          margin-bottom: 25px;
          width: fit-content;
          background-color: ${COLORS.BLUE};
          padding: 15px 10px 15px 10px;
          color: white;
          letter-spacing: 0.05rem;
        }
        @media screen and (max-width: 450px) {
          .experience_item {
            min-width: 250px;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceItem;
