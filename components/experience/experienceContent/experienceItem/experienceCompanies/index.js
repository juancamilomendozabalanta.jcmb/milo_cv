import React from "react";

//COMPONENTS
import ExperienceProjects from "./experienceProjects";
import ExperienceCompaniesTittle from "./experienceCompaniesTittle";

const ExperienceCompanies = ({ info }) => {
  return (
    <React.Fragment>
      <div className="experience_companies_box">
        <ExperienceCompaniesTittle title={info.name} url={info.url} />
        <ul className="experience_projects">
          {info.projects.map((ele, i) => {
            return <ExperienceProjects key={i} info={ele} id={i} />;
          })}
        </ul>
      </div>
      <style jsx>{`
        .experience_companies_box {
          display: flex;
          width: 100%;
          flex-flow: column;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceCompanies;
