import React, { useState } from "react";

//COMPONENTS
import ButtonLink from "../../../../../buttonLink";
import ExperienceAchievements from "./experienceAchievements";
import ExperienceTechnologies from "./experienceTechnologies";

//COLORS
import { COLORS } from "../../../../../../utils";

const ExperienceProjects = ({ info, id }) => {
  const [showAchievements, setShowAchievements] = useState(false);

  const customHeight = 100;

  const milo_epb_id = `milo_epb_${info.projectName
    .substring(0, 2)
    .toLowerCase()}_${id}`;

  const milo_ea_id = `milo_ea_${info.projectName
    .substring(0, 2)
    .toLowerCase()}_${id}`;

  const calculateNewMinHeight = () => {
    let characters = 0;
    info.achievements.forEach((element) => {
      characters += element.length;
    });

    return (characters / 100) * 16 + 260;
  };
  const callToSetShowAchievements = () => {
    const time = showAchievements ? 700 : 1000;
    setTimeout(() => {
      setShowAchievements(!showAchievements);
    }, time);
    if (!showAchievements) {
      document.getElementById(milo_epb_id).style.minHeight = `${
        calculateNewMinHeight() + customHeight
      }px`;
      document.getElementById(milo_ea_id).style.opacity = 1;
    } else {
      document.getElementById(
        milo_epb_id
      ).style.minHeight = `${customHeight}px`;
      document.getElementById(milo_ea_id).style.opacity = 0;
    }
  };

  return (
    <React.Fragment>
      <div className="experience_projects_box">
        <div id={milo_epb_id} className="experience_projects_wrap">
          <p className="experience_projects_role">{info.role}</p>
          <p className="experience_projects_mainfunction">
            {info.mainFunction}
          </p>
          <div id={milo_ea_id} className="experience_projects_achievements">
            {showAchievements && (
              <React.Fragment>
                <ExperienceAchievements info={info.achievements} />
                <ExperienceTechnologies info={info.technologies} />
              </React.Fragment>
            )}
          </div>
        </div>
        {!showAchievements ? (
          <ButtonLink
            text="READ MORE"
            handleFunction={callToSetShowAchievements}
            blColor={COLORS.DARK_GRAY}
          />
        ) : (
          <ButtonLink
            text="CLOSE"
            handleFunction={callToSetShowAchievements}
            blColor={COLORS.DARK_GRAY}
          />
        )}
      </div>
      <style jsx>{`
        .experience_projects_box {
          display: flex;
          width: 100%;
          flex-flow: column;
          justify-content: space-between;
        }
        .experience_projects_wrap {
          display: flex;
          width: 100%;
          flex-flow: column;
          min-height: ${customHeight}px;
          transition: min-height 1s;
        }
        .experience_projects_role {
          font-size: 1.2rem;
          color: ${COLORS.DARK_YELLOW};
          font-weight: bold;
          margin-bottom: 15px;
        }
        .experience_projects_mainfunction {
          font-size: 1rem;
          color: ${COLORS.DARK_GRAY};
          line-height: 1.2rem;
          margin-bottom: 20px;
        }
        .experience_projects_achievements {
          opacity: 0;
          transition: opacity 2s;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceProjects;
