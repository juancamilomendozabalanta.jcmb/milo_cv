import React from "react";

//COLORS
import { COLORS } from "../../../../../../../utils";

const ExperienceAchievements = ({ info }) => {
  return (
    <React.Fragment>
      <div className="experience_achievments_box">
        <p className="experience_achievments_tittle">Achievements</p>
        <ul className="experience_achievments_list">
          {info.map((ele, i) => {
            return (
              <li key={i} className="experience_achievments_item">
                <span className="experience_achievments_border" />
                <p  className="experience_achievments_text">
                  {ele}
                </p>
              </li>
            );
          })}
        </ul>
      </div>
      <style jsx>{`
        .experience_achievments_box {
          display: flex;
          width: 100%;
          flex-flow: column;
          color: ${COLORS.DARK_GRAY};

        }
        .experience_achievments_list {
          display: flex;
          width: 100%;
          flex-flow: column;
          margin-bottom: 10px;
        }
        .experience_achievments_border{
            border-left: 3px solid ${COLORS.GREEN};
            height: 10px;
        }
        .experience_achievments_tittle {
          font-size: 1.2rem;
          color: ${COLORS.DARK_YELLOW};
          font-weight: bold;
          margin-bottom: 15px;
        }
        .experience_achievments_item {
          margin-bottom: 10px;
          display: flex;
        }
        .experience_achievments_text{
            margin-left:5px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceAchievements;
