import React from "react";

//COMPONENTS
import TechnologiesCard from "../../../../../../technologiesCard";

//COLORS
import { COLORS } from "../../../../../../../utils";

const ExperienceTechnologies = ({ info }) => {
  return (
    <React.Fragment>
      <div className="experience_technologies_box">
        <p className="experience_technologies_tittle">Technologies</p>
        <ul className="experience_technologies_list">
          {info.map((ele, i) => {
            return <TechnologiesCard key={i} info={ele} />;
          })}
        </ul>
      </div>
      <style jsx>{`
        .experience_technologies_box {
          display: flex;
          width: 100%;
          flex-flow: column;
          color: ${COLORS.DARK_GRAY};
          margin-bottom:25px;
        }
        .experience_technologies_list {
          display: flex;
          width: 100%;
          flex-wrap: wrap;
        }
        .experience_technologies_tittle {
          font-size: 1.2rem;
          color: ${COLORS.DARK_YELLOW};
          font-weight: bold;
          margin-bottom: 15px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceTechnologies;
