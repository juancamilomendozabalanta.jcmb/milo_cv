import React from "react";

//COLORS
import { COLORS } from "../../../../../../utils";

const ExperienceCompaniesTittle = ({ title, url }) => {
  return (
    <React.Fragment>
      <div className="experience_companies_tittle">
        <a
          className="companies_tittle"
          href={url}
          target="_blank"
          rel="noopener noopener noreferrer"
        >
          {title}
        </a>
        <span className="companies_tittle_line" />
      </div>
      <style jsx>{`
        .experience_companies_tittle {
          display: flex;
          width: 100%;
          flex-flow: column;
          margin-bottom: 25px;
        }
        .companies_tittle {
          color: ${COLORS.GRAY};
          font-size: 1rem;
          font-weight: 400;
          padding-bottom: 6px;
          cursor: pointer;
          width: fit-content;
        }
        .companies_tittle:hover {
          font-weight: bold;
        }
        .companies_tittle_line {
          width: 30px;
          border-bottom: 2px solid ${COLORS.DARK_YELLOW};
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceCompaniesTittle;
