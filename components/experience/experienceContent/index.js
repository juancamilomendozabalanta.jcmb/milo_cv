import React from "react";

//HOOKS
import useExperience from "../experienceHook";

//COMPONENTS
import ExperienceItem from "./experienceItem";

const ExperienceContent = () => {
  const { experience } = useExperience();

  return (
    <React.Fragment>
      <ul className="experience_content">
        {experience.map((ele, i) => {
          return <ExperienceItem key={i} info={ele} />;
        })}
      </ul>
      <style jsx>{`
        .experience_content {
          display: flex;
          width: 100%;
          flex-wrap: wrap;
        }
      `}</style>
    </React.Fragment>
  );
};

export default ExperienceContent;
