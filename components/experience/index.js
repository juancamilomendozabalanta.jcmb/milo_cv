import React from "react";

//COMPONENTS
import ExperienceHeader from "./experienceHeader";
import ExperienceContent from "./experienceContent";

//COLORS
import { COLORS } from "../../utils";

const Experience = () => {
  return (
    <React.Fragment>
      <section className="experience_box">
        <div className="experience_wrap milo_wrap">
          <ExperienceHeader />
          <ExperienceContent />
        </div>
      </section>
      <style jsx>{`
        .experience_box {
          display: flex;
          width: 100%;
          padding: 30px 0px;
          background-color: aliceblue;
          justify-content: center;
          min-width: 350px;
        }
        .experience_wrap {
          display: flex;
          flex-flow: column;
        }
      `}</style>
    </React.Fragment>
  );
};

export default Experience;
