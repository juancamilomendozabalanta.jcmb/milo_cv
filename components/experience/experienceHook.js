import { useContext } from "react";

import { MiloContext } from "../../context";

const useExperience = () => {
  const [state, setState] = useContext(MiloContext);

  return {
    experience: state.experience,
  };
};

export default useExperience;
