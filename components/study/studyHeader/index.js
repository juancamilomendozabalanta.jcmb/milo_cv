import React from "react";

//COMPONENTS
import SectionTittle from "../../sectionTittle";

const StudyHeader = () => {
  return (
    <React.Fragment>
      <div className="study_header_box">
        <SectionTittle title={"Studying and Killing time"} />
      </div>
      <style jsx>{`
        .study_header_box {
          display: flex;
          margin: 0px 0px 20px 0px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default StudyHeader;
