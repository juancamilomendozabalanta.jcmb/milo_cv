import React from "react";

//COMPONENTS
import StudyHeader from "./studyHeader";
import StudyContent from "./studyContent";

const Study = () => {
  return (
    <React.Fragment>
      <section className="study_box">
        <div className="study_wrap milo_wrap">
          <StudyHeader />
          <StudyContent />
        </div>
      </section>
      <style jsx>{`
        .study_box {
          display: flex;
          width: 100%;
          padding: 30px 0px;
          background-color: white;
          justify-content: center;
          min-width: 350px;
        }
        .study_wrap {
          display: flex;
          flex-flow: column;
        }
      `}</style>
    </React.Fragment>
  );
};

export default Study;
