import React from "react";

//HOOKS
import useStudy from "../studyHook";

//COMPONENTS
import StudyCard from "./studyCard";

const StudyContent = () => {
  const { study } = useStudy();

  return (
    <React.Fragment>
      <ul className="study_content">
        {study.map((ele, i) => {
          return <StudyCard key={i} info={ele} />;
        })}
      </ul>
      <style jsx>{`
        .study_content {
          display: flex;
          width: 100%;
          flex-wrap: wrap;
        }
      `}</style>
    </React.Fragment>
  );
};

export default StudyContent;
