import React from "react";

//COLORS
import { COLORS } from "../../../../../../utils";

const StudyContentTitle = ({ info }) => {
  const getWidth = () => {
    return `${info.name.length * 15}px`;
  };

  return (
    <React.Fragment>
      {info.url ? (
        <a
          className="study_title"
          href={info.url}
          target="_blank"
          rel="noopener noopener noreferrer"
        >
          {info.name}
        </a>
      ) : (
        <p className="study_title">{info.name}</p>
      )}
      <style jsx>{`
        .study_title {
          display: flex;
          padding: 10px;
          margin: 20px 0px 10px 0px;
          width: ${getWidth()};
          background-color: ${COLORS.BLUE};
          color: white;
          font-weight: bold;
          font-size: 0.95rem;
          font-family: sans-serif;
          letter-spacing: 0.2rem;
          cursor: ${info.url ? "pointer" : "initial"};
          transition: all 3s;
        }
        .study_title:hover {
          width: 96.6%;
          background-color: ${COLORS.GREEN};
        }
        @media screen and (max-width: 650px) {
          .study_title:hover {
            width: 96.1%;
          }
        }
        @media screen and (max-width: 550px) {
          .study_title:hover {
            width: 95%;
          }
        }
        @media screen and (max-width: 450px) {
          .study_title:hover {
            width: 94.3%;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default StudyContentTitle;
