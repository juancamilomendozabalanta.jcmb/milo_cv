import React from "react";

//COMPONENTS
import StudyContentResume from "./studyContentResume";
import StudyContentTitle from "./studyContentTitle";

const StudyContentCard = ({ info }) => {
  const infoContentTitle = {
    name: info.name,
    url: info.url,
  };
  const infoContentResume = {
    resume: info.resume,
    state: info.state,
  };
  return (
    <React.Fragment>
      <StudyContentTitle info={infoContentTitle} />
      <StudyContentResume info={infoContentResume} />
    </React.Fragment>
  );
};

export default StudyContentCard;
