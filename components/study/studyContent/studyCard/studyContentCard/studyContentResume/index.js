import React from "react";

//COLORS
import { COLORS } from "../../../../../../utils";

const StudyContentResume = ({ info }) => {
  return (
    <React.Fragment>
      <div className="study_content_resume">
        <p className="study_resume">{info.resume}</p>
        <div className="study_state_wrap">
          <p className="study_state_title">Status</p>
          <p className="study_state">{info.state}</p>
        </div>
      </div>
      <style jsx>{`
        .study_content_resume {
          padding: 10px;
          color: ${COLORS.DARK_GRAY};
        }
        .study_resume {
          margin-bottom: 15px;
        }
        .study_state_wrap {
          display: flex;
          flex-flow: column;
          margin-bottom:5px;
        }
        .study_state_title {
          font-weight: bold;
          margin-bottom: 5px;
          font-size: 1rem;
          font-family: sans-serif;
          color: ${COLORS.DARK_YELLOW};
        }
        .study_state {
          padding-left: 5px;
          font-size: 0.9rem;
        }
      `}</style>
    </React.Fragment>
  );
};

export default StudyContentResume;
