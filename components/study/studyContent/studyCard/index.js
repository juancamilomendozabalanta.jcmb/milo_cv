import React from "react";

//COMPONENTS
import StudyContentCard from "./studyContentCard";
import StudyTechnologies from "./studyTechnologies";

const StudyCard = ({ info }) => {
  return (
    <React.Fragment>
      <div className="study_card">
        <StudyContentCard info={info} />
        <StudyTechnologies info={info.technologies} />
      </div>
      <style jsx>{`
        .study_card {
          display: flex;
          width: 100%;
          min-width: 350px;
          min-height: 150px;
          background-color: white;
          margin: 10px;
          border-radius: 5px;
          flex-flow: column;
          box-shadow: 0px 0px 3px 0px #3d4b5830;
        }
        @media screen and (max-width: 450px) {
          .study_card {
            padding-right: 0px;
            margin: 10px 0px;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default StudyCard;
