import React from "react";

//COLORS
import { COLORS } from "../../../../../../utils";

const StudyTechnologiesCard = ({ info }) => {
  return (
    <React.Fragment>
      <div className="study_technologies_card_box">
        <p className="study_technologies_card_tittle">{info.name}</p>
        <ul className="study_technologies_card_list">
          {info.list.map((ele, i) => {
            return (
              <li key={i} className="study_technologies_card_item">
                <p className="study_technologies_card_text">{ele}</p>
              </li>
            );
          })}
        </ul>
      </div>
      <style jsx>{`
        .study_technologies_card_box {
          display: flex;
          width: 100%;
          flex-flow: column;
          border: 1px solid ${COLORS.BLUE};
          margin: 5px;
          padding: 10px 0px;
          font-size: 0.8rem;
        }
        .study_technologies_card_list {
          display: flex;
          width: fit-content;
          flex-flow: column;
          padding: 0px 10px;
        }
        .study_technologies_card_tittle {
          color: ${COLORS.BLUE};
          font-weight: bold;
          margin-bottom: 10px;
          padding: 0px 10px 10px 10px;
          border-bottom: 1px solid ${COLORS.BLUE};
        }
        .study_technologies_card_item {
          margin-bottom: 10px;
          display: flex;
          color: ${COLORS.GRAY};
        }
        .study_technologies_card_text {
          margin-left: 5px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default StudyTechnologiesCard;
