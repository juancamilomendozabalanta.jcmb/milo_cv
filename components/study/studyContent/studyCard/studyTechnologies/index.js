import React from "react";

//COMPONENTS
import StudyTechnologiesCard from "./studyTechnologiesCard";

//COLORS
import { COLORS } from "../../../../../utils";

const StudyTechnologies = ({ info }) => {
  return (
    <React.Fragment>
      <div className="study_technologies_box">
        <p className="study_technologies_tittle">Technologies</p>
        <ul className="study_technologies_list">
          {info.map((ele, i) => {
            return <StudyTechnologiesCard key={i} info={ele} />;
          })}
        </ul>
      </div>
      <style jsx>{`
        .study_technologies_box {
          display: flex;
          flex-flow: column;
          color: ${COLORS.DARK_GRAY};
          padding: 0px 10px 10px 10px;
        }
        .study_technologies_list {
          display: flex;
          width: 100%;
        }
        .study_technologies_tittle {
          font-weight: bold;
          margin-bottom: 5px;
          font-size: 1rem;
          font-family: sans-serif;
          color: ${COLORS.DARK_YELLOW};
          margin-bottom: 5px;
        }
        @media screen and (max-width: 550px) {
          .study_technologies_list {
            flex-wrap: wrap;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default StudyTechnologies;
