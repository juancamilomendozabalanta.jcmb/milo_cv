import { useContext } from "react";

import { MiloContext } from "../../context";

const useStudy = () => {
  const [state, setState] = useContext(MiloContext);

  return {
    study: state.study,
  };
};

export default useStudy;
