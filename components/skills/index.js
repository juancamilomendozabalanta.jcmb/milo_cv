import React from "react";

//COMPONENTS
import SkillsHeader from "./skillsHeader";
import SkillsContent from "./skillsContent";

const Skills = () => {
  return (
    <React.Fragment>
      <section className="skills_box">
        <div className="skills_wrap milo_wrap">
          <SkillsHeader />
          <SkillsContent />
        </div>
      </section>
      <style jsx>{`
        .skills_box {
          display: flex;
          width: 100%;
          padding: 30px 0px;
          background-color: aliceblue;
          justify-content: center;
        }
        .skills_wrap {
          display: flex;
          flex-flow: column;
        }
      `}</style>
    </React.Fragment>
  );
};

export default Skills;
