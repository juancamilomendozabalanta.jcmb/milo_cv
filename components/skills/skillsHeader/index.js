import React from "react";

//COMPONENTS
import SectionTittle from "../../sectionTittle";

const SkillsHeader = () => {
  return (
    <React.Fragment>
      <div className="skills_header_box">
        <SectionTittle title={"Skills"} />
      </div>
      <style jsx>{`
        .skills_header_box {
          display: flex;
          margin: 0px 0px 20px 0px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default SkillsHeader;
