import React from "react";

//HOOKS
import useSkills from "../skillsHook";

//COMPONENTS
import SkillsCard from "./skillsCard";

const SkillsContent = () => {
  const { skills } = useSkills();

  return (
    <React.Fragment>
      <ul className="skills_content">
        {skills.technical.map((ele, i) => {
          return <SkillsCard key={i} info={ele}/>;
        })}
      </ul>
      <style jsx>{`
        .skills_content {
          display: flex;
          width: 100%;
          flex-wrap: wrap;
        }
      `}</style>
    </React.Fragment>
  );
};

export default SkillsContent;
