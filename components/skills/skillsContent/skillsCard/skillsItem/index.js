import React from "react";

//COLORS
import { COLORS } from "../../../../../utils";

const SkillItem = ({ info }) => {
  return (
    <React.Fragment>
      <div className="skill_item_box">
        <p className="skill_item_name">{info}</p>
      </div>
      <style jsx>{`
        .skill_item_box {
          display: flex;
          padding: 10px;
          background-color: white;
          margin: 10px;
          flex-flow: column;
          font-size: 0.9rem;
          border-radius: 4px;
          box-shadow: 0px 0px 3px 0px #3d4b5830;
        }
        .skill_item_name {
          font-size: 0.9rem;
          color:${COLORS.DARK_GRAY}; 
        }
      `}</style>
    </React.Fragment>
  );
};

export default SkillItem;
