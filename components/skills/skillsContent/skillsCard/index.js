import React from "react";

//COMPONENTS
import SkillsItem from "./skillsItem";

const SkillsCard = ({ info }) => {
  return (
    <React.Fragment>
      <div className="skills_card">
        <p className="skills_name">{info.name}</p>
        <ul className={`skills_list`}>
          {info.list.map((ele, i) => {
            return <SkillsItem key={i} info={ele} />;
          })}
        </ul>
      </div>
      <style jsx>{`
        .skills_card {
          display: flex;
          width: 100%;
          margin: 10px;
          flex-flow: column;
          background-color: white;
          border-radius: 5px;
        }
        .skills_list {
          display: flex;
          width: 100%;
          flex-wrap: wrap;
          margin-bottom: 10px;
        }
        .skills_name {
          font-weight: bold;
          margin-bottom: 10px;
          border-top-left-radius: 5px;
          border-top-right-radius: 5px;
          font-size: 1rem;
          font-family: sans-serif;
          border-bottom: 2px solid ${info.color};
          padding: 10px;
          letter-spacing: 0.05rem;
          animation: setcolor 10s infinite alternate;
        }
        @keyframes setcolor {
          to {
            border-bottom: 2px solid green;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default SkillsCard;
