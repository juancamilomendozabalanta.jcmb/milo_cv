import { useContext } from "react";

import { MiloContext } from "../../context";

const useSkills = () => {
  const [state, setState] = useContext(MiloContext);

  return {
    skills: state.skills,
  };
};

export default useSkills;
