import { useContext } from "react";

import { MiloContext } from "../../context";

const useEducation = () => {
  const [state, setState] = useContext(MiloContext);

  return {
    education: state.education,
  };
};

export default useEducation;
