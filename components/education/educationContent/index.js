import React from "react";

//HOOKS
import useEducation from "../educationHook";

//COMPONENTS
import EducationCard from "./educationCard";

const EducationContent = () => {
  const { education } = useEducation();

  return (
    <React.Fragment>
      <ul className="education_content">
        {education.map((ele, i) => {
          return <EducationCard key={i} info={ele} index={i+1}/>;
        })}
      </ul>
      <style jsx>{`
        .education_content {
          display: flex;
          width: 100%;
          flex-wrap: wrap;
          margin-top:40px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default EducationContent;
