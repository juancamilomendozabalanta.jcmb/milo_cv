import React from "react";

//COMPONENTS
import EducationItem from "./educationItem";

//COLORS
import { COLORS } from "../../../../utils";

const EducationCard = ({ info, index }) => {
  return (
    <React.Fragment>
      <div className="education_card">
        <div className="education_year_wrap">
          <p className={`education_year ${index % 2 === 0 ? "even_year" : ""}`}>
            {info.year}
          </p>
        </div>
        <ul
          className={`education_list ${index % 2 === 0 ? "even_number" : ""}`}
        >
          {info.titles.map((ele, i) => {
            return <EducationItem key={i} info={ele} />;
          })}
        </ul>
      </div>
      <style jsx>{`
        .education_card {
          display: flex;
          width: 100%;
          padding: 0px 20px 20px 20px;
          margin: 10px;
          flex-flow: column;
        }
        .education_year_wrap {
          width: 100%;
          display: flex;
          border-bottom: 1px solid ${COLORS.BLUE};
          margin-bottom: 15px;
          position: relative;
        }
        .education_list {
          display: flex;
          width: 100%;
          flex-wrap: wrap;
        }
        .even_number {
          justify-content: end;
        }
        .education_year {
          font-weight: bold;
          font-size: 0.95rem;
          font-family: sans-serif;
          width: fit-content;
          background-color: ${COLORS.BLUE};
          padding: 5px;
          color: white;
          letter-spacing: 0.05rem;
          position: absolute;
          top: -25px;
          left: 0px;
          animation: setpositionleft 10s infinite alternate;
        }
        .even_year {
          left: 492px;
          animation: setpositionright 10s infinite alternate;
        }
        @keyframes setpositionleft {
          to {
            left: 492px;
          }
        }
        @keyframes setpositionright {
          to {
            left: 0px;
          }
        }
        @media screen and (max-width: 650px) {
          .even_year {
            left: 395px;
          }
          @keyframes setpositionleft {
            to {
              left: 395px;
            }
          }
        }
        @media screen and (max-width: 550px) {
          .even_year {
            left: 295px;
          }
          @keyframes setpositionleft {
            to {
              left: 295px;
            }
          }
        }
        @media screen and (max-width: 450px) {
          .even_year {
            left: 244px;
          }
          @keyframes setpositionleft {
            to {
              left: 244px;
            }
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default EducationCard;
