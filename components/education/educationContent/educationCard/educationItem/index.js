import React from "react";

//COLORS
import { COLORS } from "../../../../../utils";

const EducationItem = ({ info }) => {
  return (
    <React.Fragment>
      <div className="education_item_box">
        <p className="education_item_name">{info.name}</p>
        <p className="education_item_place">{info.place}</p>
      </div>
      <style jsx>{`
        .education_item_box {
          display: flex;
          padding: 10px;
          background-color: white;
          margin: 10px;
          flex-flow: column;
          min-width: 240px;
          font-size:0.9rem;
          border-radius: 4px;
          box-shadow: 0px 0px 3px 0px #3d4b5830;  
          border-left: 4px solid ${COLORS.GREEN};      
        }
        .education_item_name{
            font-weight:bold;
            margin-bottom: 5px;
        }
        .education_item_place{
          color:${COLORS.DARK_GRAY}; 
        }
      `}</style>
    </React.Fragment>
  );
};

export default EducationItem;
