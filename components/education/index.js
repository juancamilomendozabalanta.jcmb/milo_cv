import React from "react";

//COMPONENTS
import EducationHeader from "./educationHeader";
import EducationContent from "./educationContent";

const Education = () => {
  return (
    <React.Fragment>
      <section className="education_box">
        <div className="education_wrap milo_wrap">
          <EducationHeader />
          <EducationContent />
        </div>
      </section>
      <style jsx>{`
        .education_box {
          display: flex;
          width: 100%;
          padding: 30px 0px;
          background-color: white;
          justify-content: center;
        }
        .education_wrap {
          display: flex;
          flex-flow: column;
        }
      `}</style>
    </React.Fragment>
  );
};

export default Education;
