import React from "react";

//COMPONENTS
import SectionTittle from "../../sectionTittle";

const EducationHeader = () => {
  return (
    <React.Fragment>
      <div className="education_header_box">
        <SectionTittle title={"Education"} />
      </div>
      <style jsx>{`
        .education_header_box {
          display: flex;
          margin: 0px 0px 20px 0px;
        }
      `}</style>
    </React.Fragment>
  );
};

export default EducationHeader;
