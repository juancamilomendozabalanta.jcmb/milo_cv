import React from "react";

//COLORS
import { COLORS } from "../../utils";

const TechnologiesCard = ({ info }) => {
  return (
    <React.Fragment>
      <div className="technologies_card_box">
        <p className="technologies_card_tittle">{info.name}</p>
        <ul className="technologies_card_list">
          {info.list.map((ele, i) => {
            return (
              <p key={i} className="technologies_card_item">
                {ele}
              </p>
            );
          })}
        </ul>
      </div>
      <style jsx>{`
        .technologies_card_box {
          display: flex;
          min-width: 200px;
          flex-flow: column;
          color: ${COLORS.BLUE};
          margin: 5px;
          border-radius: 4px;
          border: 1px solid ${COLORS.BLUE};
        }
        .technologies_card_tittle {
          font-size: 1rem;
          margin-bottom: 10px;
          font-family: sans-serif;
          letter-spacing: 0.2rem;
          padding: 10px;
          border-bottom: 1px solid;
        }
        .technologies_card_list {
          display: flex;
          width: 100%;
          padding: 0px 10px 10px 10px;
          flex-wrap: wrap;
        }
        .technologies_card_item {
          font-size: 0.8rem;
          padding: 10px;
          margin: 5px;
          border-radius: 15px;
          color:white;
          background-color: ${COLORS.GREEN};
        }
      `}</style>
    </React.Fragment>
  );
};

export default TechnologiesCard;
