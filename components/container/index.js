import React from "react";

//COMPONENTS
import HeaderMilo from "../header";
import Experience from "../experience";
import Education from "../education";
import Skills from "../skills";
import Study from "../study";
import ContactAndHobbie from "../contactAndHobbie";

const Container = () => {
  return (
    <React.Fragment>
      <div className="container_box">
        <HeaderMilo />
        <Experience />
        <Education />
        <Skills />
        <Study />
        <ContactAndHobbie />
      </div>
      <style jsx>{`
        .container_box {
          display: flex;
          flex-flow: column;
          width: 100%;
          align-items: center;
        }
      `}</style>
    </React.Fragment>
  );
};

export default Container;
