import React, { useState } from "react";

const initialContext = {
  me: {
    firstname: "Juan Camilo",
    lastname: "Mendoza Balanta",
    role: "Full Stack Developer",
    nickname: "Milo",
    photo: "/static/milo.png",
    resume:
      "I am Software Engineer offering six years of experience with teamwork skills, excellent interpersonal relationships and ease and willingness to learn.",
  },
  education: [
    {
      year: "2023",
      titles: [
        {
          name: "AWS Cloud Technical Essentials",
          place: "",
        },
        {
          name: "OPEN CV",
          place: "",
        },
        {
          name: "Anaconda Prompt",
          place: "",
        },
      ],
    },
    {
      year: "2022",
      titles: [
        {
          name: "Python",
          place: "",
        },
      ],
    },
    {
      year: "2021",
      titles: [
        {
          name: "Typescript: De Cero a Experto 2021",
          place: "Globant Udemy",
        },
        {
          name: "Softskills - Leadership & management",
          place: "Globant Udemy",
        },
      ],
    },
    {
      year: "2020",
      titles: [
        {
          name: "Desing thinking",
          place: "Platzi",
        },
        {
          name: "Camino emprendedor",
          place: "Platzi",
        },
      ],
    },
    {
      year: "2019",
      titles: [
        {
          name: "Advanced Google Analytics course",
          place: "Google",
        },
        {
          name: "Google Analytics for begginer",
          place: "Google",
        },
        {
          name: "Node from zero to expert",
          place: "Udemy",
        },
      ],
    },
    {
      year: "2018",
      titles: [
        {
          name: "Training - Environment Of Web Applications",
          place: "FORMATIC (MINTIC) - San BuenaVentura University",
        },
      ],
    },
    {
      year: "2017",
      titles: [
        {
          name: "Telematic Engineer",
          place: "ICESI University",
        },
      ],
    },
    {
      year: "2016",
      titles: [
        {
          name: "Specialized Training On Business Strategy",
          place: "MINTIC and Fedesoft - November 2016.",
        },
      ],
    },
  ],
  experience: [
    {
      year: "2023",
      companies: [
        {
          name: "ENCORA",
          url: "https://www.encora.com/",
          projects: [
            {
              role: "Senior Software Engineer",
              projectName: "Price-spider",
              mainFunction: "Development of new features and support team",
              achievements: [
               "Improve crawler functionality for the scraping services",
               "Improve the analyzing images functionality for the image study service",
               "Propose and implement the distribution of our application in an architecture by continents that will avoid the blocking of some websites due to the origin of the requests",
              ],
              technologies: [
                {
                  name: "Back",
                  list: ["NODE JS", "PYTHON", "SHELL"],
                },
                {
                  name: "Databases",
                  list: ["POSTGRES", "BIGQUERY"],
                },
                {
                  name: "Platforms",
                  list: ["GITHUB", "GOOGLE CLOUD"],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      year: "2022",
      companies: [
        {
          name: "ENCORA",
          url: "https://www.encora.com/",
          projects: [
            {
              role: "Senior Software Engineer",
              projectName: "Guarddog",
              mainFunction: "Development of new features",
              achievements: [
                "We have created a new feature that through the functionalities of Google Cloud IoT Core, allows you to send configuration messages to the devices that are linked to your account.",
                "Improvements in services that make use of hacker tools to avoid cybersecurity attacks such as Deauthentication, the Evil Twin and the ARP SPOOF attack",
                "Create new services in the core of the application, which used python as the main language, stored the data in a Mongo database and was consumed from an API built with Node JS and Typescript",
                "Create new features in the Front of the application, which is made in React",
              ],
              technologies: [
                {
                  name: "Front",
                  list: ["REACT JS"],
                },
                {
                  name: "Back",
                  list: ["NODE JS", "PYTHON", "SHELL"],
                },
                {
                  name: "Databases",
                  list: ["MONGO DB"],
                },
                {
                  name: "Platforms",
                  list: ["GITHUB", "GOOGLE CLOUD", "MONGO DB COMPASS"],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      year: "2021",
      companies: [
        {
          name: "GLOBANT",
          url: "https://www.globant.com/about?utm_source=google&utm_medium=cpc&utm_campaign=mkt_us_all_sem_lgn_cpc_11092020_tl0773_business-hacking&gclid=Cj0KCQjwsqmEBhDiARIsANV8H3bthrtGPOqE6KtXkTraLTP9vLAgdO5IT7cBI4uBWUO4UEN-moCNUi4aAvbNEALw_wcB",
          projects: [
            {
              role: "Technical Leader",
              projectName: "Survey module",
              mainFunction:
                "Develop an api called survey module that allow integration with any api or front end of supervielle bank.",
              achievements: [
                "Design of a database with a modular and bridge architecture",
              ],
              technologies: [
                {
                  name: "Back",
                  list: ["NODE JS", "EXPRESS JS"],
                },
                {
                  name: "Databases",
                  list: ["MONGO DB"],
                },
                {
                  name: "Platforms",
                  list: ["GITHUB", "MONGO DB COMPASS"],
                },
              ],
            },
            {
              role: "Technical Leader",
              projectName: "Hub Virtual",
              mainFunction:
                "Develop the virtual hub application that allows Supervielle Bank customers to take a virtual shift and be attended by an executive through a video call.",
              achievements: [
                "Develop the Front End of the virtual hub.",
                "Develop the Back End of the virtual hub.",
              ],
              technologies: [
                {
                  name: "Front",
                  list: ["NEXT JS", "JSX", "CSS"],
                },
                {
                  name: "Back",
                  list: ["NODE JS", "EXPRESS JS"],
                },
                {
                  name: "Databases",
                  list: ["MONGO DB"],
                },
                {
                  name: "Platforms",
                  list: [
                    "GITHUB",
                    "CONSUL",
                    "RUNDECK",
                    "RANCHER",
                    "GOOGLE ANALYTICS",
                    "AMAZON S3",
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      year: "2020",
      companies: [
        {
          name: "GLOBANT",
          url: "https://www.globant.com/about?utm_source=google&utm_medium=cpc&utm_campaign=mkt_us_all_sem_lgn_cpc_11092020_tl0773_business-hacking&gclid=Cj0KCQjwsqmEBhDiARIsANV8H3bthrtGPOqE6KtXkTraLTP9vLAgdO5IT7cBI4uBWUO4UEN-moCNUi4aAvbNEALw_wcB",
          projects: [
            {
              role: "Web UI Developer",
              projectName: "Front Gate Tickets",
              mainFunction: "Development of new features for clients.",
              achievements: ["Improvements in the webs sites of the client."],
              technologies: [
                {
                  name: "Front",
                  list: ["ANGULAR JS"],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      year: "2019",
      companies: [
        {
          name: "CONDOR LABS",
          url: "http://condorlabs.io/",
          projects: [
            {
              role: "Javascript Developer",
              projectName: "Wallet",
              mainFunction:
                "Development of new features in both frontend and backend sides, improvement and support to EverCheck Wallet app.",
              achievements: [
                "Improvements in EverCheck Wallet app.",
                "Implements Google Analytics to EverCheck Wallet app.",
                "Improvements in the support team.",
              ],
              technologies: [
                {
                  name: "Front",
                  list: ["REACT JS"],
                },
                {
                  name: "Back",
                  list: ["NODE JS", "EXPRESS JS"],
                },
                {
                  name: "Databases",
                  list: ["MYSQL"],
                },
                {
                  name: "Platforms",
                  list: ["GITHUB"],
                },
              ],
            },
          ],
        },
      ],
    },
  ],
  skills: {
    technical: [
      {
        name: "Markup",
        color: "#FF0000",
        list: ["HTML", "PUG", "JSX"],
      },
      {
        name: "Backend",
        color: "#FF8700",
        list: ["NODE JS", "GRAPHQL"],
      },
      {
        name: "Css",
        color: "#FFD300",
        list: ["SASS", "STYLUS"],
      },
      {
        name: "Database",
        color: "#0AFF99",
        list: ["MYSQL", "MONGO DB", "REDIS"],
      },
      {
        name: "Bundlers",
        color: "#0AEFFF",
        list: ["GULP", "WEBPACK"],
      },
      {
        name: "Test",
        color: "#147DF5",
        list: ["SINON", "JEST"],
      },
      {
        name: "Frameworks",
        color: "#580AFF",
        list: ["ANGULAR JS", "REACT JS", "NEXT JS", "GRAPHQL", "TYPE SCRIPT"],
      },
      {
        name: "Cloud",
        color: "#580AFF",
        list: ["GOOGLE CLOUD", "AWS"],
      },
      {
        name: "Analytics",
        color: "#BE0AFF",
        list: ["GOOGLE ANALYTICS - certified"],
      },
    ],
  },
  study: [
    {
      name: "TRAVELERS",
      url: "",
      resume: "It's a web application to allow manage the PHAMA of your trip",
      state: "Working in DEV",
      technologies: [
        {
          name: "FRONT",
          list: ["REACT JS", "GRAPHQL", "SCSS"],
        },
        {
          name: "BACK",
          list: ["NODE JS", "GRAPHQL"],
        },
        {
          name: "DATA BASE",
          list: ["MYSQL"],
        },
        {
          name: "PLATFORMS",
          list: ["Not yet"],
        },
      ],
    },
    {
      name: "MOVIES",
      url: "https://movies-web-site.herokuapp.com/",
      resume: "It's a web applications to managment your movies",
      state: "Working in PROD",
      technologies: [
        {
          name: "FRONT",
          list: ["REACT JS", "SCSS"],
        },
        {
          name: "BACK",
          list: ["NODE JS", "EXPRESS JS"],
        },
        {
          name: "DATA BASE",
          list: ["MONGO DB"],
        },
        {
          name: "PLATFORMS",
          list: ["HEROKU", "MONGO ATLAS"],
        },
      ],
    },
  ],
  contacts: [
    {
      logo: "MailIcon",
      name: "Gmail",
      url: "https://mail.google.com/mail/u/0/?fs=1&to=juancamilomendozabalanta.jcmb@gmail.com&tf=cm",
    },
    {
      logo: "GitHubIcon",
      name: "GitHub",
      url: "https://github.com/JuanCamiloMendozaBalanta",
    },
    {
      logo: "LinkedInIcon",
      name: "LinkedIn",
      url: "https://www.linkedin.com/in/juancamilomendozabalanta/?locale=en_US",
    },
  ],
  hoobies: [
    {
      name: "Soccer",
      logo: "SportsSoccerIcon",
    },
    {
      name: "Eat",
      logo: "FastfoodIcon",
    },
    {
      name: "Movies",
      logo: "LocalMoviesIcon",
    },
  ],
};

const MiloContext = React.createContext([{}, () => {}]);

const MiloProvider = (props) => {
  const [state, setState] = useState(initialContext);
  return <MiloContext.Provider value={[state, setState]} {...props} />;
};

export { MiloContext, MiloProvider, initialContext };
