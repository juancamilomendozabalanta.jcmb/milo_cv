const COLORS = {
  DARK_YELLOW: "#f79b13",
  BLUE: "#073763ff",
  GRAY: "#4f5d75",
  DARK_GRAY: "#3d4b58",
  LIGHT_GRAY: "#f2f7ffc4",
  GREEN:"#3D9970"
};

export { COLORS };
