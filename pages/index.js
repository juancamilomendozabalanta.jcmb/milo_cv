import React from "react";
import Head from "next/head";
//COMPONENTS
import Container from "@/components/container";

export default function Main() {
  return (
    <React.Fragment>
      <Head>
        <title>I'Milo</title>
      </Head>
      <Container></Container>
    </React.Fragment>
  );
}
