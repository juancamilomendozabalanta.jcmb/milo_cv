import { MiloProvider } from "../context";

//STYLE
import "@/styles/main.css";

const MyApp = ({ Component, pageProps }) => {
  return (
    <MiloProvider>
      <Component {...pageProps} />
    </MiloProvider>
  );
};

export default MyApp;
